# README #

This version of the app is test, and it uses some Architecture Components like ViewModel, LiveData, and other lifecycle-aware classes. It's based on the todo-mvvm-viewbinding, which uses the View Binding Library to display data and bind UI elements to actions.


### Topics ###
* Retrofit2 
* okhttp3
* Dagger2
* ViewBinding
* RxJava2
* Paging Library
* Glide
* Spark
###  ###
