package com.test.coinranking.data.source.coin.remote;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.test.coinranking.base.RxWrapper;
import com.test.coinranking.data.ResourceState;
import com.test.coinranking.data.Status;
import com.test.coinranking.data.model.Coin;
import com.test.coinranking.data.source.coin.remote.request.GetCoinsParams;
import com.test.coinranking.data.source.coin.remote.response.CoinsResponse;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

/**
 * Loads list depend of user action :
 *  loadInitial (open screen or refresh);
 *  loadAfter (when user scroll down);
 *
 */
public class CoinPagingDataSource extends PageKeyedDataSource<Integer, Coin> {

    private final int PAGE_SIZE = 20;

    private final CompositeDisposable compositeDisposable;
    private final CoinRepository repository;
    private final GetCoinsParams.OrderBy orderBy;
    private final GetCoinsParams.OrderDirection orderDirection;

    private int offset;

    MutableLiveData<ResourceState> state = new MutableLiveData<ResourceState>();

    public MutableLiveData<ResourceState> getState() {
        return state;
    }

    public CoinPagingDataSource(CompositeDisposable compositeDisposable, CoinRepository repository, GetCoinsParams.OrderBy orderBy, GetCoinsParams.OrderDirection orderDirection) {
        this.compositeDisposable = compositeDisposable;
        this.repository = repository;
        this.orderBy = orderBy;
        this.orderDirection = orderDirection;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Coin> callback) {
        GetCoinsParams getCoinsParams = new GetCoinsParams.Builder()
                .limit(PAGE_SIZE)
                .offset(0)
                .orderBy(orderBy)
                .orderDirection(orderDirection)
                .build();
        compositeDisposable.add(RxWrapper.compositeWrapper(repository.getCoins(getCoinsParams)).doOnTerminate(terminateAction()).doOnSubscribe(loadingState())
                .subscribe(
                        new Consumer<CoinsResponse>() {
                            @Override
                            public void accept(CoinsResponse coinsResponse) throws Exception {
                                offset = coinsResponse.getData().getCoins().size();
                                callback.onResult(coinsResponse.getData().getCoins(), 0, offset);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                updateState(ResourceState.error(throwable));
                            }
                        }
                ));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Coin> callback) {
        //Doesn't use because api does't support it
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Coin> callback) {
        GetCoinsParams getCoinsParams = new GetCoinsParams.Builder()
                .limit(PAGE_SIZE)
                .offset(offset)
                .orderBy(GetCoinsParams.OrderBy.MARKET_CUP)
                .orderDirection(GetCoinsParams.OrderDirection.DESC)
                .build();

        compositeDisposable.add(RxWrapper.compositeWrapper(repository.getCoins(getCoinsParams)).doOnTerminate(terminateAction()).doOnSubscribe(loadingState())
                .subscribe(
                        new Consumer<CoinsResponse>() {
                            @Override
                            public void accept(CoinsResponse coinsResponse) throws Exception {

                                offset = offset + coinsResponse.getData().getCoins().size();
                                callback.onResult(coinsResponse.getData().getCoins(), offset);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                updateState(ResourceState.error(throwable));
                            }
                        }
                ));
    }

    private void updateState(ResourceState state) {
        this.state.postValue(state);
    }

    private Action terminateAction() {
        return new Action() {
            @Override
            public void run() throws Exception {
                updateState(ResourceState.success());
            }
        };
    }

    private Consumer<Disposable> loadingState() {
        return response -> updateState(ResourceState.loading());
    }

}
