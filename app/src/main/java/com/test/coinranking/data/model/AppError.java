package com.test.coinranking.data.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;


import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;


/**
 * A class that handle throwable to get message and code.
 */
public class AppError {
    //UNKNOWN ERROR by default
    private int code = -1;
    private String message;

    public AppError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public AppError(int code) {
        this.code = code;
    }

    public AppError(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static AppError handleThrowable(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            return new AppError(exception.code(), parseHttpException(exception));
        } else {
            return new AppError(throwable.getLocalizedMessage());
        }
    }



    private static String parseHttpException(HttpException exception){
        String output = exception.getLocalizedMessage();
        try {
            Response<?> response = exception.response();
            if (response != null) {
                ResponseBody body = response.errorBody();
                if (body != null) {
                    String responseData = body.string();
                    JsonObject json = new JsonParser().parse(responseData).getAsJsonObject();
                    if (json.has("message")) {
                        JsonElement field = json.get("message");
                        if (field != null && !field.isJsonNull())
                            output = field.getAsString();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

}

