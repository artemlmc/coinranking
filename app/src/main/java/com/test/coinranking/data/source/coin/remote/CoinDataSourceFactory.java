package com.test.coinranking.data.source.coin.remote;


import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.test.coinranking.data.model.Coin;
import com.test.coinranking.data.source.coin.remote.request.GetCoinsParams;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Provide CoinPagingDataSource with required params like repository and orders
 */
public class CoinDataSourceFactory extends DataSource.Factory<Integer, Coin> {

    private final CompositeDisposable compositeDisposable;
    private final CoinRepository repository;
    private GetCoinsParams.OrderBy orderBy;
    private GetCoinsParams.OrderDirection orderDirection;


    public MutableLiveData<CoinPagingDataSource> lvDataSource() {
        return lvDataSource;
    }

    public CoinDataSourceFactory(CompositeDisposable compositeDisposable, CoinRepository repository, GetCoinsParams.OrderBy orderBy, GetCoinsParams.OrderDirection orderDirection) {
        this.compositeDisposable = compositeDisposable;
        this.repository = repository;
        this.orderBy = orderBy;
        this.orderDirection = orderDirection;

    }

    public void setOrderBy(GetCoinsParams.OrderBy orderBy) {
        this.orderBy = orderBy;
    }

    public void setOrderDirection(GetCoinsParams.OrderDirection orderDirection) {
        this.orderDirection = orderDirection;
    }

    public MutableLiveData<CoinPagingDataSource> lvDataSource = new MutableLiveData<CoinPagingDataSource>();

    @NonNull
    @Override
    public DataSource<Integer, Coin> create() {
        CoinPagingDataSource  dataSource = new CoinPagingDataSource(compositeDisposable, repository, orderBy, orderDirection);
        lvDataSource.postValue(dataSource);
        return dataSource;
    }
}
