package com.test.coinranking.data;

import com.test.coinranking.data.model.AppError;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * A class that holds a AppError with its loading status.
 */
public class ResourceState {
    private final Status status;
    private AppError appError;

    private ResourceState(Status status, AppError appError) {
        this.status = status;
        this.appError = appError;
    }

    private ResourceState(Status status) {
        this.status = status;
    }

    public static ResourceState success() {
        return new ResourceState(Status.DONE);
    }

    public static ResourceState error(Throwable throwable) {
        AppError appError = AppError.handleThrowable(throwable);
        return new ResourceState(Status.ERROR, appError);
    }

    public static ResourceState loading() {
        return new ResourceState(Status.LOADING);
    }

    public static ResourceState initDone() {
        return new ResourceState(Status.INIT_DONE);
    }

    public Status getStatus() {
        return status;
    }

    public AppError getAppError() {
        return appError;
    }
}
