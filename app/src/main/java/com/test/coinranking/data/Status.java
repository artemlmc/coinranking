package com.test.coinranking.data;

public enum Status {
    DONE, LOADING, INIT_DONE, ERROR
}
