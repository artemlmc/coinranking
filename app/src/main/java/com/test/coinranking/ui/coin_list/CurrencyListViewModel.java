package com.test.coinranking.ui.coin_list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.test.coinranking.base.SingleLiveEvent;
import com.test.coinranking.base.util.ConnectionHelper;
import com.test.coinranking.data.ResourceState;
import com.test.coinranking.data.Status;
import com.test.coinranking.data.model.Coin;
import com.test.coinranking.data.source.coin.remote.CoinDataSourceFactory;
import com.test.coinranking.data.source.coin.remote.CoinPagingDataSource;
import com.test.coinranking.data.source.coin.remote.CoinRepository;
import com.test.coinranking.data.source.coin.remote.request.GetCoinsParams;
import com.test.coinranking.ui.common.BaseViewModel;

import javax.inject.Inject;

public class CurrencyListViewModel extends BaseViewModel {

    //Paging loading
    private final CoinDataSourceFactory coinDataSourceFactory;
    private final LiveData<PagedList<Coin>> lvCoins;
    //List orders
    private final MutableLiveData<GetCoinsParams.OrderBy> lvOrderBy = new MutableLiveData<>(GetCoinsParams.OrderBy.MARKET_CUP);
    private final MutableLiveData<GetCoinsParams.OrderDirection> lvOrderDirection = new MutableLiveData<>(GetCoinsParams.OrderDirection.DESC);
    private final MutableLiveData<Boolean> lvReload = new MutableLiveData<>(true);

    LiveData<PagedList<Coin>> lvCoins() {
        return lvCoins;
    }

    MutableLiveData<Boolean> lvReload() {
        return lvReload;
    }

    MutableLiveData<GetCoinsParams.OrderBy> orderBy() {
        return lvOrderBy;
    }
    MutableLiveData<GetCoinsParams.OrderDirection> orderDirection() {
        return lvOrderDirection;
    }

    public LiveData<ResourceState> getState() {
        return Transformations.switchMap(this.coinDataSourceFactory.lvDataSource, CoinPagingDataSource::getState);
    }

    @Inject
    public CurrencyListViewModel(CoinRepository repository, ConnectionHelper connectionHelper) {
        super(connectionHelper);
        this.coinDataSourceFactory = new CoinDataSourceFactory(getCompositeDisposable(), repository, lvOrderBy.getValue(), lvOrderDirection.getValue());
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(20) // size of page
                .setInitialLoadSizeHint(20) // count of items with will be loading by init load
                .setEnablePlaceholders(false)
                .build();

        lvCoins = new LivePagedListBuilder<>(this.coinDataSourceFactory, config).build();
    }

    private void reload() {
        CoinPagingDataSource paging = coinDataSourceFactory.lvDataSource().getValue();
        if (paging != null) {
            paging.invalidate();
            lvReload.setValue(true);
        }
    }

    boolean isListEmpty() {
        PagedList<Coin> coins = lvCoins.getValue();
        return coins == null || coins.isEmpty();
    }

    boolean isEnableProgress() {
         Boolean isReload = lvReload.getValue();
         return isListEmpty() || (isReload != null && isReload);
    }


    /**
     * set order and call invalidate DataSource to refresh list
     */
    void setOrderBy(GetCoinsParams.OrderBy value) {
        if (isInternetAvailable()) {
            lvOrderBy.setValue(value);
            lvOrderDirection.setValue(GetCoinsParams.OrderDirection.DESC);
            coinDataSourceFactory.setOrderBy(value);
            reload();
        }
    }

    /**
     * setup opposite order and call invalidate DataSource to refresh list
     */
    void setOppositeOrderDirection() {
        if (isInternetAvailable()) {
            lvOrderDirection.setValue(GetCoinsParams.OrderDirection.getOpposite(lvOrderDirection.getValue()));
            coinDataSourceFactory.setOrderDirection(lvOrderDirection.getValue());
            reload();
        }
    }


}
