package com.test.coinranking.ui.coin_list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.test.coinranking.R;
import com.test.coinranking.data.ResourceState;
import com.test.coinranking.data.model.Coin;
import com.test.coinranking.data.source.coin.remote.request.GetCoinsParams;
import com.test.coinranking.databinding.FragmentCoinListBinding;


import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

import static androidx.recyclerview.widget.LinearLayoutManager.VERTICAL;

public class CoinListFragment extends DaggerFragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private CurrencyListViewModel viewModel;

    FragmentCoinListBinding binding;
    private CoinController controller;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = FragmentCoinListBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this, viewModelFactory).get(CurrencyListViewModel.class);
        viewModel.lvCoins().observe(getViewLifecycleOwner(), observeCoins());
        viewModel.getState().observe(getViewLifecycleOwner(), observeState());
        viewModel.lvMessage().observeOn(getViewLifecycleOwner(), this::showAlert);
        controller = new CoinController(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(binding.rvCoin.getContext(),
                VERTICAL);
        binding.rvCoin.addItemDecoration(dividerItemDecoration);
        binding.rvCoin.setController(controller);

        binding.marketCap.setOnClickListener(v -> {
            if (viewModel.orderBy().getValue() == GetCoinsParams.OrderBy.MARKET_CUP) {
                viewModel.setOppositeOrderDirection();
            } else {
                viewModel.setOrderBy(GetCoinsParams.OrderBy.MARKET_CUP);
            }

        });
        binding.price.setOnClickListener(v -> {
            if (viewModel.orderBy().getValue() == GetCoinsParams.OrderBy.PRICE) {
                viewModel.setOppositeOrderDirection();
            } else {
                viewModel.setOrderBy(GetCoinsParams.OrderBy.PRICE);
            }
        });
        binding.twentyFourHVolume.setOnClickListener(v -> {
            if (viewModel.orderBy().getValue() == GetCoinsParams.OrderBy.TWENTY_FOUR_H_VOLUME) {
                viewModel.setOppositeOrderDirection();
            } else {
                viewModel.setOrderBy(GetCoinsParams.OrderBy.TWENTY_FOUR_H_VOLUME);
            }
        });
    }

    private Observer<PagedList<Coin>> observeCoins() {
        return list -> controller.submitList(list);
    }

    private void showAlert(String message) {
        AlertDialog ad = new AlertDialog.Builder(getContext())
                .setTitle(R.string.error)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                .create();

        ad.show();
    }

    private Observer<ResourceState> observeState() {
        return state -> {
            switch (state.getStatus()) {
                case DONE:
                case INIT_DONE:
                    if (!viewModel.isListEmpty()) binding.rvCoin.setVisibility(View.VISIBLE);
                    viewModel.lvReload().setValue(false);
                    binding.clProgress.setVisibility(View.GONE);
                    if (viewModel.isListEmpty()) binding.emptyText.setVisibility(View.VISIBLE);
                    break;
                case LOADING:
                    binding.emptyText.setVisibility(View.GONE);
                    if (viewModel.isEnableProgress()) binding.clProgress.setVisibility(View.VISIBLE);
                    break;
                case ERROR:
                    if (viewModel.isListEmpty()) binding.emptyText.setVisibility(View.VISIBLE);
                    binding.clProgress.setVisibility(View.GONE);
                    showAlert(state.getAppError().getMessage());
                    break;
            }
        };
    }

}
