package com.test.coinranking.ui.coin_list;

import android.util.Log;

import com.robinhood.spark.SparkAdapter;

import java.util.ArrayList;


/**
 * adapter to populate spain line
 */
public class SparkCoinsAdapter extends SparkAdapter {

    private final String TAG = SparkCoinsAdapter.class.getCanonicalName();

    public SparkCoinsAdapter(ArrayList<String> yData) {
        this.yData = yData;
    }

    private final ArrayList<String> yData;

    /**
     * @return the number of points to be drawn
     */
    @Override
    public int getCount() {
        return yData.size();
    }

    /**
     * @param index
     * @return the object at the given index
     */
    @Override
    public Object getItem(int index) {
        try {
            return Double.parseDouble(yData.get(index));
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        return 0f;
    }

    /**
     * @param index
     * @return the float representation of the Y value of the point at the given index.
     */
    @Override
    public float getY(int index) {
        try {
            return Float.parseFloat(yData.get(index));
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        return 0f;
    }
}
