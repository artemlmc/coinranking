package com.test.coinranking.base.util;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.lifecycle.LiveData;

import com.test.coinranking.R;

import javax.inject.Inject;

public class ConnectionHelper extends LiveData<Boolean> {

    private final ConnectivityManager connectivityManager;
    private final String message;

    @Inject
    public ConnectionHelper(Application application) {
        this.connectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.message = application.getString(R.string.internet_connection);
    }

    public boolean isInternetAvailable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network networkCapabilities = connectivityManager.getActiveNetwork();
            if (networkCapabilities == null)
                return false;
            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(networkCapabilities);
            if (actNw == null)
                return false;
            return actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET);
        } else {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo == null)
                return false;
            switch (networkInfo.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                case ConnectivityManager.TYPE_MOBILE:
                case ConnectivityManager.TYPE_ETHERNET:
                    return true;
                default:
                    return false;

            }
        }
    }

    public String getMessage() {
        return message;
    }
}
